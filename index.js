const canvas = document.getElementById("canvas");
const ctx = canvas.getContext("2d");

const maze = createMaze()

const player = createPlayer(maze.entryPoint().x + 1, maze.entryPoint().y - 19);
player.draw();

document.onkeydown = e => {
    if (e.keyCode === 38) {
        player.moveUp(maze);
    } else if (e.keyCode === 40) {
        player.moveDown(maze);
    } else if (e.keyCode === 39) {
        player.moveRight(maze);
    } else if (e.keyCode === 37) {
        player.moveLeft(maze);
    }
}

function createPlayer(x, y) {
    const position = {
        x,
        y,
    }
    const size = 15;
    const draw = function () {
        const DOMURL = window.URL || window.webkitURL || window;
        const img1 = new Image();
        const svg = new Blob([`<svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                width="${size}" height="${size}"
                viewBox="0 0 224 224"
                style=" fill:#000000;"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,224v-224h224v224z" fill="none"></path><g fill="#184e78"><g id="surface1"><path d="M114.24,8.96c-13.58,0 -24.64,11.06 -24.64,24.64c0,13.58 11.06,24.64 24.64,24.64c13.58,0 24.64,-11.06 24.64,-24.64c0,-13.58 -11.06,-24.64 -24.64,-24.64zM91.84,62.72c-13.58,0 -24.64,11.06 -24.64,24.64v56c0,4.9525 4.025,8.96 8.96,8.96c4.935,0 8.96,-4.0075 8.96,-8.96v-42.7c0,-1.2075 1.0325,-2.24 2.24,-2.24c1.2075,0 2.24,1.0325 2.24,2.24v102.34c0,7 3.7625,12.04 10.78,12.04c6.6325,0 11.62,-5.145 11.62,-12.04v-57.54c0,-1.2425 0.9975,-2.24 2.24,-2.24c1.2425,0 2.24,0.9975 2.24,2.24v58.8c0.0175,0.035 0.1225,-0.035 0.14,0c0.6125,6.195 5.285,10.78 11.48,10.78c7,0 10.78,-5.04 10.78,-12.04v-101.5c0,-1.2075 1.0325,-2.24 2.24,-2.24c1.2075,0 2.24,1.0325 2.24,2.24v41.86c0,4.9525 4.025,8.96 8.96,8.96c4.935,0 8.96,-4.0075 8.96,-8.96v-56c0,-13.58 -11.06,-24.64 -24.64,-24.64z"></path></g></g></g></svg>`], { type: 'image/svg+xml' });
        const url = DOMURL.createObjectURL(svg);

        img1.onload = function () {
            ctx.drawImage(img1, position.x, position.y);
            DOMURL.revokeObjectURL(url);
        }
        img1.src = url;
    }

    function moveVerticlly(expectedPosition, collider) {
        if (!collider.isColliding(position.x, expectedPosition, position.x + size, expectedPosition + size)) {
            ctx.clearRect(position.x, position.y, size, size)
            position.y = expectedPosition;
            draw();
        }
    }

    function moveHorizontly(expectedPosition, collider) {
        if (!collider.isColliding(expectedPosition, position.y, expectedPosition + size, position.y + size)) {
            ctx.clearRect(position.x, position.y, size, size)
            position.x = expectedPosition;
            draw();
        }
    }

    return {
        draw,
        moveUp: function (collider) {
            moveVerticlly(position.y - 10, collider);
        },
        moveDown: function (collider) {
            moveVerticlly(position.y + 10, collider);
        },
        moveLeft: function (collider) {
            moveHorizontly(position.x - 10, collider);
        },
        moveRight: function (collider) {
            moveHorizontly(position.x + 10, collider);
        },
    }
}

function createMaze() {
    const areas = [createArea(0, 0, 600, 600)];
    const walls = [];
    let startingPoint = 0;
    for (let k = 0; k < 20; k++) {
        const areasLength = areas.length;
        for (let i = startingPoint; i < areasLength; i++) {
            const area = areas[i];
            area.splitArea();
            area.getWalls().forEach(wall => walls.push(wall));
            area.getAreas().forEach(newArea => areas.push(newArea));
        }
        startingPoint = areasLength;
    }

    function calculateEntry(y) {
        let entryWalls = walls.filter(wall => wall.y2 === y && wall.x >= 200 && wall.x2 <= 400)
            .sort((a, b) => a.x - b.x);

        if (entryWalls.length < 2) {
            return {
                x: 300,
                x2: 320,
                y
            }
        }

        return {
            x: entryWalls[0].x,
            x2: entryWalls[1].x,
            y
        }
    }

    const entryPoint = calculateEntry(600);
    const exitPoint = calculateEntry(0);

    walls.push(
        createWall(0, 0, exitPoint.x, 0),
        createWall(exitPoint.x2, 0, 600, 0),
        createWall(600, 0, 600, 600),
        createWall(0, 600, entryPoint.x, 600),
        createWall(entryPoint.x2, 600, 600, 600),
        createWall(0, 600, 0, 0),
    )
    walls.forEach(wall => wall.draw())

    const entry = createWall(entryPoint.x, 600, entryPoint.x2, 600);
    const exit = createWall(exitPoint.x, 0, exitPoint.x2, 0);
    return {
        entryPoint: function () {
            return entryPoint;
        },
        isColliding: function (colliderX, colliderY, colliderX2, colliderY2) {
            for (let i = 0; i < walls.length; i++) {
                if (
                    entry.isColliding(colliderX, colliderY, colliderX2, colliderY2) ||
                    exit.isColliding(colliderX, colliderY, colliderX2, colliderY2) ||
                    walls[i].isColliding(colliderX, colliderY, colliderX2, colliderY2)
                ) {
                    return true;
                }
            }
            return false;
        }
    }
}

function createArea(x, y, x2, y2) {
    const resolution = 20;
    const areas = [];
    const height = y2 - y;
    const width = x2 - x;
    const walls = [];


    function randomStartingPoint(max, min) {
        return Math.floor((Math.random() * (max - min) + min) / resolution) * resolution;
    }

    function splitHorizontly() {
        const point = randomStartingPoint(y2 - resolution, y + resolution);
        const point2 = randomStartingPoint(x2, x);
        walls.push(
            createWall(x, point, point2, point),
            createWall(point2 + resolution, point, x2, point)
        );

        areas.push(
            createArea(x, y, x2, point),
            createArea(x, point, x2, y2)
        )
    }
    function splitVertically() {
        const point = randomStartingPoint(x2 - resolution, x + resolution);
        const point2 = randomStartingPoint(y2, y);
        walls.push(
            createWall(point, y, point, point2),
            createWall(point, point2 + resolution, point, y2)
        );

        areas.push(
            createArea(x, y, point, y2),
            createArea(point, y, x2, y2)
        )
    }

    const isResolutionSize = x2 - x === resolution || y2 - y === resolution;

    return {
        splitArea: function () {
            if (isResolutionSize) {
                return;
            } else if (height < width) {
                splitVertically()
            } else if (height > width) {
                splitHorizontly()
            } else {
                const randNumber = Math.floor(Math.random() * (3 - 1) + 1);
                randNumber === 1 ? splitHorizontly() : splitVertically();
            }
        },

        getAreas: function () {
            return areas;
        },

        getWalls: function () {
            return walls;
        }
    }
}

function createWall(x, y, x2, y2) {

    let isColliding;
    if (y === y2) {
        isColliding = function (colliderX, colliderY, colliderX2, colliderY2) {
            return (colliderX >= x && colliderX <= x2 ||
                colliderX2 >= x && colliderX2 <= x2) &&
                colliderY2 >= y && colliderY <= y
        }
    } else if (x === x2) {
        isColliding = function (colliderX, colliderY, colliderX2, colliderY2) {
            return (colliderY >= y && colliderY <= y2 ||
                colliderY2 >= y && colliderY2 <= y2) &&
                colliderX2 >= x && colliderX <= x
        }
    }
    return {
        draw: function () {
            ctx.beginPath();
            ctx.moveTo(x, y);
            ctx.lineTo(x2, y2);
            ctx.stroke();
        },
        isColliding,
        x,
        y,
        x2,
        y2
    }
}
